" Turn on syntax highlighting
syntax on

" Show line numbers
set number

" Encoding
set encoding=utf-8

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch

" Theme
"colorscheme murphy
colorscheme elflord

" Highlight current line
set cursorline
