HISTSIZE=10000
HISTFILESIZE=20000
HISTCONTROL="ignoredups"
HISTIGNORE="&:ls:[bf]g:exit"

shopt -s autocd
shopt -s cdspell
shopt -s dirspell
shopt -s histappend
PROMT_COMMAND='history -a'

if [ -f ~/.aliases ]; then
    . ~/.aliases
fi
if [ -f ~/.color ]; then
    . ~/.color
fi

export PS1="\[\033[38;5;81m\]\u\[${nc}\][\[${nc}\]\[\033[38;5;81m\]\h] \t \[${nc}\]\[${blue}\]\w\[${nc}\]\n>"
