<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class RecoveryPasswordEmailJob extends Job
{
    protected User $User;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $User)
    {
        $this->User = $User;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.forgot_password', ['token' => $this->User->password_resets], function($message) {
            $message->to($this->User->email);
            $message->subject('Reset Your Password');
        });
    }
}
