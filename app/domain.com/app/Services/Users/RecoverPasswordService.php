<?php

namespace App\Services\Users;

use App\Jobs\RecoveryPasswordEmailJob;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Exception;
use Illuminate\Support\Str;

class RecoverPasswordService
{
    protected UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws Exception
     */
    public function createRecoveryPasswordActions(string $email)
    {
        $User = $this->userRepository->getUserByEmail($email);
        if ($User === null) {
            return false;
        }
        $User->password_resets = Str::random(40);
        $User->save();

        dispatch(new RecoveryPasswordEmailJob($User));

        return true;
    }
}
