<?php

namespace App\Services\Users;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

class CreateUserService
{
    protected UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(array $user): User
    {
        return $this->userRepository->create($user);
    }
}
