<?php

namespace App\Services\Company;

use App\Repositories\Interfaces\CompanyRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class CreateCompanyService
{
    protected CompanyRepositoryInterface $companyRepository;

    /**
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function create(array $company)
    {
        $company['user_id'] = Auth::guard('api')->user()->id;
        unset($company['api_token']);
        return $this->companyRepository->create($company);
    }
}
