<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class ChangePasswordController extends Controller
{
    protected UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validate($request, [
            'recoveryToken' => 'required|min:40',
            'newPassword' => 'required|min:8',
            'newPasswordConfirm' => 'required|min:8',
        ]);

        $User = $this->userRepository->getUserByPasswordRecoveryToken(
            $request->only(['recoveryToken', 'newPassword', 'newPasswordConfirm'])
        );

        return response()->json([
            'token' => $User->api_token,
        ]);
    }
}
