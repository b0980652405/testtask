<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\Users\CreateUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RegisterUserController extends Controller
{
    protected CreateUserService $userService;

    /**
     * @param CreateUserService $userService
     */
    public function __construct(CreateUserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'phone' => 'required|min:10',
        ]);

        $User = $this->userService->create($request->only(['first_name', 'last_name', 'email', 'password', 'phone']));

        return response()->json(
            ['token' => $User->api_token],
            201
        );
    }
}
