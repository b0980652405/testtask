<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\Users\RecoverPasswordService;
use Illuminate\Http\Request;

class RecoverPasswordController extends Controller
{
    protected RecoverPasswordService $userService;

    /**
     * @param RecoverPasswordService $userService
     */
    public function __construct(RecoverPasswordService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $result = $this->userService->createRecoveryPasswordActions($request->get('email'));
        if ($result === false){
            return response()->json([
                'result' => 'Email not found'
            ], 400);
        }

        return response()->json([
            'result' => 'Please search remember token for next step in email'
        ]);
    }
}
