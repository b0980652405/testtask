<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Auth;

class GetCompanyController extends Controller
{
    /**
     * @throws Exception
     */
    public function __invoke()
    {
        if (Auth::guard('api')->check()) {
            $User = Auth::guard('api')->user();

            return response()->json([
                'companies' => $User->companies()->get()->toArray()
            ]);
        }

        throw new Exception('Auth is wrong', 400);
    }
}
