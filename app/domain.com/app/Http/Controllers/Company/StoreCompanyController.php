<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Services\Company\CreateCompanyService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class StoreCompanyController extends Controller
{
    protected CreateCompanyService $companyService;

    /**
     * @param CreateCompanyService $companyService
     */
    public function __construct(CreateCompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function __invoke(Request $request): JsonResponse
    {
        if (Auth::guard('api')->check()) {
            $this->validate($request, [
                'title' => 'required|min:3',
                'phone' => 'required|min:10',
                'description' => 'string',
            ]);

            return response()->json([
                'company' => $this->companyService->create($request->all())
            ]);
        }

        throw new Exception('Auth is wrong', 400);
    }
}
