<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Mail\MailManager;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Register the application's mailer.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('mailer', function ($app) {
            $mailer = new MailManager($app);
            $mailer->setDefaultDriver('smtp');
            $mailer->setSwiftMailer($app['swift.mailer']);
            return $mailer;
        });
    }
}
