<?php

namespace App\Repositories\Eloquent;

use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{

    public function create(array $company)
    {
        return Company::create([
            'user_id' => $company['user_id'],
            'title' => $company['title'],
            'phone' => $company['phone'],
            'description' => $company['description'],
        ]);
    }
}
