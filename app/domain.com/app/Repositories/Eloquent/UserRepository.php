<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Exception;
use Illuminate\Support\Str;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @param array $user
     * @return mixed
     */
    public function create(array $user): mixed
    {
        return User::create([
            'api_token' => Str::random(100),
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'password' => app('hash')->make($user['password']),
            'phone' => $user['phone'],
        ]);
    }

    /**
     * @param array $user
     * @return string|false
     */
    public function getUserTokenByCredentials(array $user): string|false
    {
        $User = User::where('email', $user['email'])->first();

        if ($User && app('hash')->check($user['password'], $User->password)) {
            $User->api_token = Str::random(100);
            $User->save();

            return $User->api_token;
        }

        return false;
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function getUserByEmail(string $email): mixed
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param array $changePasswordData
     * @return User
     * @throws Exception
     */
    public function getUserByPasswordRecoveryToken(array $changePasswordData): User
    {
        $User = User::where('password_resets', $changePasswordData['recoveryToken'])->first();
        if ($User === null) {
            throw new Exception('User not found', 400);
        }
        $User->password_resets = null;
        $User->password = app('hash')->make($changePasswordData['newPassword']);
        $User->api_token = Str::random(100);
        $User->save();

        return $User;
    }
}
