<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Password Recovery</title>
</head>
<body>
<h1>Password Recovery</h1>
<p>Dear user,</p>
<p>You have requested to reset your password. Please click the following link to continue:</p>
<p>{{ $token }}</p>
<p>If you did not make this request, please ignore this email.</p>
<p>Best regards,</p>
<p>The Support Team</p>
</body>
</html>
