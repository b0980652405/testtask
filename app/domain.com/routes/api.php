<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('register',   ['uses' => 'User\RegisterUserController@__invoke']);
        $router->post('sign-in',    ['uses' => 'User\LoginUserController@__invoke']);
        $router->post('recover-password',     ['uses' => 'User\RecoverPasswordController@__invoke']);
        $router->patch('recover-password',    ['uses' => 'User\ChangePasswordController@__invoke']);
        $router->get('companies',   ['uses' => 'Company\GetCompanyController@__invoke']);
        $router->post('companies',  ['uses' => 'Company\StoreCompanyController@__invoke']);
    });
});
