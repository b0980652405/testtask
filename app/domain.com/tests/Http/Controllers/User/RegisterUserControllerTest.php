<?php


use Faker\Factory;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;

class RegisterUserControllerTest extends TestCase
{
    private Generator $faker;

    public function setUp(): void
    {
        $this->faker = Factory::create();
    }

    /**
     * @return Generator|\Generator
     */
    protected function dataUsersForRegistrationProvider(): Generator|\Generator
    {
        $this->faker = Factory::create();

        yield 'valid' => [
            'data' => [
                "first_name" => $this->faker->firstName,
                "last_name" => $this->faker->lastName,
                "email" => $this->faker->email,
                "password" => "ThisIsVerySecurityPassword",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => $this->faker->numberBetween(11111111111, 9999999999)
            ],
            "status" => 201
        ];

        yield 'with empty first_name' => [
            'data' => [
                "first_name" => '',
                "last_name" => $this->faker->lastName,
                "email" => $this->faker->email,
                "password" => "ThisIsVerySecurityPassword",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => $this->faker->numberBetween(11111111111, 9999999999)
            ],
            "status" => 422
        ];

        yield 'with empty last_name' => [
            'data' => [
                "first_name" => $this->faker->firstName,
                "last_name" => '',
                "email" => $this->faker->email,
                "password" => "ThisIsVerySecurityPassword",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => $this->faker->numberBetween(11111111111, 9999999999)
            ],
            "status" => 422
        ];

        yield 'with empty email' => [
            'data' => [
                "first_name" => $this->faker->firstName,
                "last_name" => $this->faker->lastName,
                "email" => "",
                "password" => "ThisIsVerySecurityPassword",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => $this->faker->numberBetween(11111111111, 9999999999)
            ],
            "status" => 422
        ];

        yield 'with empty password' => [
            'data' => [
                "first_name" => $this->faker->firstName,
                "last_name" => $this->faker->lastName,
                "email" => $this->faker->email,
                "password" => "",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => $this->faker->numberBetween(11111111111, 9999999999)
            ],
            "status" => 422
        ];
        yield 'with empty phone' => [
            'data' => [
                "first_name" => $this->faker->firstName(),
                "last_name" => $this->faker->lastName,
                "email" => $this->faker->email,
                "password" => "ThisIsVerySecurityPassword",
                "password_confirmation" => "ThisIsVerySecurityPassword",
                "phone" => ""
            ],
            "status" => 422
        ];
    }

    /**
     * @param array $dataForRequest
     * @param int $statusCode
     * @return void
     * @throws GuzzleException
     * @dataProvider dataUsersForRegistrationProvider
     */
    public function testRegistrationUser(array $dataForRequest, int $statusCode): void
    {
        if ($statusCode !== 200 && $statusCode !== 201) {
            $this->expectException(ClientException::class);
            $this->expectExceptionCode($statusCode);
        }

        $Client = new Client(['verify' => false ]);
        $response = $Client->request('POST', $_ENV['HOST'] . '/user/register', [
            'json' => $dataForRequest,
        ]);

        $this->assertEquals($statusCode, $response->getStatusCode());

        $content = json_decode($response->getBody()->getContents(), true);
        $this->assertArrayHasKey('token', $content);
        $this->assertNotEmpty($content['token']);
    }
}
