<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Create 10 users
        $users = User::factory(10)->create();

        // For each user, create a company with user_id equal to the user's ID
        $users->each(function ($user) use ($faker) {
            Company::factory()->create([
                'user_id' => $user->id,
                'title' => $faker->word,
                'description' => $faker->word,
                'phone' => $faker->numberBetween(11111111111, 9999999999)
            ]);
        });
    }
}
