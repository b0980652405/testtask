<?php

namespace Database\Factories;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * @return array|mixed[]
     */
    public function definition(): array
    {
        $faker = Faker::create();
        return [
            'api_token' => Str::random(100),
            'first_name' => $faker->word,
            'last_name' => $faker->lastName,
            'email' => $faker->unique()->safeEmail,
            'password' => app('hash')->make('password'), // password
            'phone' => $faker->numberBetween(11111111111, 9999999999)
        ];
    }
}
