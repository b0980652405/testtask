<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * @return array|mixed[]
     */
    public function definition(): array
    {
        $faker = Faker::create();
        return [
            'title' => $faker->word,
            'description' => $faker->word,
            'phone' => $faker->numberBetween(11111111111, 9999999999)
        ];
    }
}
